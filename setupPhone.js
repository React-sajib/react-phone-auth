import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
const FirebaseConfig = {
  apiKey: 'AIzaSyAh3-zzZ-PTB5yokENURrWdAEk_7Pai8VE',

  authDomain: 'daily-expense-2f363.firebaseapp.com',

  projectId: 'daily-expense-2f363',

  storageBucket: 'daily-expense-2f363.appspot.com',

  messagingSenderId: '747505830281',

  appId: '1:747505830281:web:9339ab63f62abfe0f7f958',

  measurementId: 'G-YES5YF7G0F',
};

if (!firebase.apps.length) {
  firebase.initializeApp(FirebaseConfig);
}

export default () => {
  return {
    firebase,
    auth,
  };
};
