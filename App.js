import React, {Component} from 'react';
import {View, Text, TextInput, ScrollView, Button, alert} from 'react-native';
// import auth from '@react-native-firebase/auth'
import FirebaseSetup from './setupPhone';
const {auth} = FirebaseSetup();
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      confirm: '',
      confirmResult: '',
      userId: '',
    };
  }

  // If null, no SMS has been sent
  // const [confirm, setConfirm] = useState(null);

  // const [code, setCode] = useState('');

  // Handle the button press
  signInWithPhoneNumber = () => {
    const confirmation = auth()
      .signInWithPhoneNumber(this.state.phone)
      .then(res => {
        console.warn(res, 'check jphone');
        this.setState({confirmResult: res});
      })
      .catch(e => {});
  };

  confirmCode = () => {
    this.state.confirmResult
      .confirm(this.state.confirm)
      .then(user => {
        this.setState({userId: user.uid});
        console.log('verified');
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    if (this.state.confirmResult == '') {
      return (
        <View style={{flex: 1, paddingHorizontal: 10, paddingVertical: 10}}>
          <Text style={{textAlign: 'center', marginTop: 25}}>
            {' '}
            Phone Verify{' '}
          </Text>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{marginTop: 20}}>
              <TextInput
                style={{
                  backgroundColor: '#fff',
                  borderWidth: 1,
                  borderColor: '#ddd',
                  fontSize: 14,
                  paddingVertical: 20,
                  paddingHorizontal: 20,
                  fontFamily: 'Roboto-Regular',
                }}
                placeholder="phone number"
                keyboardType="phone-pad"
                value={this.state.phone}
                autoCapitalize="none"
                onChangeText={text => this.setState({phone: text})}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Button
                onPress={() => this.signInWithPhoneNumber()}
                title="send"
                color="green"
                accessibilityLabel="Learn more about this purple button"
              />
            </View>
          </ScrollView>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, paddingHorizontal: 10, paddingVertical: 10}}>
          <Text style={{textAlign: 'center', marginTop: 25}}>
            {' '}
            Phone Verify{' '}
          </Text>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{marginTop: 20}}>
              <TextInput
                style={{
                  backgroundColor: '#fff',
                  borderWidth: 1,
                  borderColor: '#ddd',
                  fontSize: 14,
                  paddingVertical: 20,
                  paddingHorizontal: 20,
                  fontFamily: 'Roboto-Regular',
                }}
                placeholder="Confirm code"
                keyboardType="default"
                value={this.state.confirm}
                autoCapitalize="none"
                onChangeText={text => this.setState({confirm: text})}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Button
                onPress={() => this.confirmCode()}
                title="send"
                color="green"
                accessibilityLabel="Learn more about this purple button"
              />
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

export default App;
